Redux saga follows an event driven system for managing side effects in any application.

My key takeaways:

- When a user performs an event an action is dispatched
- Redux saga is setup to have watcher sagas and worker sagas
- watcher sagas act as event listeners and trigger the worker sagas when their events are fired
- The fact that it is event based enables Redux saga operate in a reactive manner.
- Worker sagas handle managing side effects.
- Async network requests can be made within worker sagas
- Since Redux saga uses JavaScripts generator functions, it is able to handle asynchronous code in and easy to reason manner
- Within the generator functions actions are dispatched with relevant data for reducers to make use of to update the application store.

key resource: https://hackernoon.com/redux-saga-tutorial-for-beginners-and-dog-lovers-aa69a17db645
